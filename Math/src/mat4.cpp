#include"mat4.h"

cgmath::mat4::mat4() : n{0.0f} {

}
cgmath::mat4::mat4(float diagonal) : n{ {diagonal,0.0f,0.0f,0.0f},{0.0f,diagonal,0.0f,0.0f},{0.0f,0.0f,diagonal,0.0f},{0.0f,0.0f,0.0f,diagonal} } {

}
cgmath::mat4::mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d) : n{ {a.x,a.y,a.z,a.w},{b.x,b.y,b.z,b.w},{c.x,c.y,c.z,c.w},{d.x,d.y,d.z,d.w} } {

}
cgmath::vec4& cgmath::mat4::operator[](int column) {
	return reinterpret_cast<vec4&>(n[column][0], n[column][0], n[column][0], n[column][0]);
}
const cgmath::vec4& cgmath::mat4::operator[](int column)const {
	return reinterpret_cast<const vec4&>(n[column][0], n[column][0], n[column][0], n[column][0]);
}
bool cgmath::mat4::operator==(const mat4& m)const {
	bool pass = true;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (m[i][j] != n[i][j]) {
				pass = false;
				break;
			}
		}
	}
	return pass;
}
cgmath::mat4 cgmath::mat4::inverse(const mat4& m) {
	float extend[4][4 * 2];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			extend[i][j] = m[i][j];
			if (i == j) {
				extend[i][j + 4] = 1;
			}
			else {
				extend[i][j + 4] = 0;
			}
		}
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (j != i) {
				float tempPiv = extend[j][i] / extend[i][i];
				for (int k = 0; k < 4 * 2; k++) {
					extend[j][k] -= extend[i][k] * tempPiv;
				}
			}
		}
	}
	for (int i = 0; i < 4; i++) {
		float pivot = extend[i][i];
		for (int j = 0; j < 4 * 2; j++) {
			extend[i][j] /= pivot;
		}
	}
	return mat4(vec4(extend[0][4], extend[0][5], extend[0][6], extend[0][7]),
		vec4 (extend[1][4], extend[1][5], extend[1][6], extend[1][7]),
		vec4 (extend[2][4], extend[2][5], extend[2][6], extend[2][7]),
		vec4 (extend[3][4], extend[3][5], extend[3][6], extend[3][7]));
}

