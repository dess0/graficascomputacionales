#include"mat3.h"


cgmath::mat3::mat3() : n{ 0.0f } {

}
cgmath::mat3::mat3(float diagonal) : n{ {diagonal,0.0f,0.0f}, {0.0f,diagonal,0.0f}, {0.0f,0.0f,diagonal} } {

}
cgmath::mat3::mat3(const vec3& a, const vec3& b, const vec3& c) : n{ {a.x,a.y,a.z},{b.x,b.y,b.z},{c.x,c.y,c.z} } {

}
cgmath::vec3& cgmath::mat3::operator[](int column) {
	return reinterpret_cast<vec3&>(n[column][0], n[column][0], n[column][0]);
}
const cgmath::vec3& cgmath::mat3::operator[](int column)const {
	return reinterpret_cast<const vec3&>(n[column][0], n[column][0], n[column][0]);
}
bool cgmath::mat3::operator==(const mat3& m)const {
	bool pass = true;
	for (int i = 0; i < 3; i++) {
		for(int j=0;j<3;j++){
			if (m[i][j] != n[i][j]) {
				pass = false;
				break;
			}
		}
	}
	return pass;
}
float cgmath::mat3::determinant(const mat3& m) {
	float part1 = m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]);
	float part2 = m[1][0] * (m[0][1] * m[2][2] - m[0][2] * m[2][1]);
	float part3 = m[2][0] * (m[0][1] * m[1][2] - m[0][2] * m[1][1]);
	return part1 - part2 + part3;
}
cgmath::mat3 cgmath::mat3::inverse(const mat3& m) {
	float det = determinant(m);
	float invDet = 1 / det;

	float a = (m[1][1] * m[2][2] - m[2][1] * m[1][2])*invDet;
	float b = (m[2][0] * m[1][2] - m[1][0] * m[2][2])*invDet;
	float c = (m[1][0] * m[2][1] - m[2][0] * m[1][1])*invDet;
	float d = (m[2][1] * m[0][2] - m[0][1] * m[2][2])*invDet;
	float e = (m[0][0] * m[2][2] - m[2][0] * m[0][2])*invDet;
	float f = (m[2][0] * m[0][1] - m[0][0] * m[2][1])*invDet;
	float g = (m[0][1] * m[1][2] - m[1][1] * m[0][2])*invDet;
	float h = (m[1][0] * m[0][2] - m[0][0] * m[1][2])*invDet;
	float i = (m[0][0] * m[1][1] - m[1][0] * m[0][1])*invDet;

	return mat3(vec3(a, d, g), vec3(b, e, h), vec3(c, f, i));
}
cgmath::mat3 cgmath::mat3::transpose(const mat3& m) {
	return mat3(vec3(m[0][0], m[1][0], m[2][0]), vec3(m[0][1], m[1][1], m[2][1]), vec3(m[0][2], m[1][2], m[2][2]));
}