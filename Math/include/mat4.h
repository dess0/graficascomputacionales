#pragma once

#include<ostream>
#include<iostream>
#include"vec4.h"

namespace cgmath {
	class mat4 {
	private:
		float n[4][4];
	public:
		mat4();
		mat4(float diagonal);
		mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d);

		vec4& operator[](int column);
		const vec4& operator[](int column)const;
		bool operator==(const mat4& m)const;

		static mat4 inverse(const mat4& m);

		friend std::ostream& operator<<(std::ostream& os, const mat4& m) {
			return os << m[0][0] << " " << m[1][0] << " " << m[2][0] << " " << m[3][0] << "\n"
				<< m[0][1] << " " << m[1][1] << " " << m[2][1] << " " << m[3][1] << "\n"
				<< m[0][2] << " " << m[1][2] << " " << m[2][2] << " " << m[3][2] << "\n"
				<< m[0][3] << " " << m[1][3] << " " << m[2][3] << " " << m[3][3];
		}
	};
	inline vec4 operator*(const mat4& m, const vec4& v) {
		float x = (m[0][0] * v.x) + (m[1][0] * v.y) + (m[2][0] * v.z) + (m[3][0] * v.w);
		float y = (m[0][1] * v.x) + (m[1][1] * v.y) + (m[2][1] * v.z) + (m[3][1] * v.w);
		float z = (m[0][2] * v.x) + (m[1][2] * v.y) + (m[2][2] * v.z) + (m[3][2] * v.w);
		float w = (m[0][3] * v.x) + (m[1][3] * v.y) + (m[2][3] * v.z) + (m[3][3] * v.w);
		return vec4(x, y, z, w);
	}
	inline mat4 operator*(const mat4& m1, const mat4& m2) {
		float res[4][4] = {};
		for (int k = 0; k < 4; k++) {
			for (int i = 0; i < 4; i++) {
				float sumMult = 0.0;
				for (int j = 0; j < 4; j++) {
					sumMult += (m1[j][i] * m2[k][j]);
				}
				res[k][i] = sumMult;
			}
		}
		return mat4(vec4(res[0][0], res[0][1], res[0][2], res[0][3]),
			vec4(res[1][0], res[1][1], res[1][2], res[1][3]),
			vec4(res[2][0], res[2][1], res[2][2], res[2][3]),
			vec4(res[3][0], res[3][1], res[3][2], res[3][3]));
	}
}