#pragma once

#include<ostream>
#include<iostream>
#include "vec3.h"

namespace cgmath {
	class mat3 {
	private:
		float n[3][3];
		
	public:
		mat3();
		mat3(float diagonal);
		mat3(const vec3& a, const vec3& b, const vec3& c);

		vec3& operator[](int column);
		const vec3& operator[](int column)const;
		bool operator==(const mat3& m)const;

		static float determinant(const mat3& m);
		static mat3 inverse(const mat3& m);
		static mat3 transpose(const mat3& m);

		friend std::ostream& operator<<(std::ostream& os, const mat3& m) {
			return os << m[0][0] << " " << m[1][0] << " " << m[2][0] << "\n"
				<< m[0][1] << " " << m[1][1] << " " << m[2][1] << "\n"
				<< m[0][2] << " " << m[1][2] << " " << m[2][2];
		}
	};
	inline vec3 operator*(const mat3& m, const vec3& v) {
		float x = (m[0][0] * v.x) + (m[1][0] * v.y) + (m[2][0] * v.z);
		float y = (m[0][1] * v.x) + (m[1][1] * v.y) + (m[2][1] * v.z);
		float z = (m[0][2] * v.x) + (m[1][2] * v.y) + (m[2][2] * v.z);
		return vec3(x, y, z);
	}
	inline mat3 operator*(const mat3& m1, const mat3& m2) {
		float res[3][3] = {};
		for(int k = 0; k < 3; k++) {
			for (int i = 0; i < 3; i++) {
				float sumMult = 0.0;
				for (int j = 0; j < 3; j++) {
					sumMult += (m1[j][i] * m2[k][j]);
				}
				res[k][i] = sumMult;
			}
		}
		return mat3(vec3(res[0][0], res[0][1], res[0][2]), 
			vec3(res[1][0], res[1][1], res[1][2]), 
			vec3(res[2][0], res[2][1], res[2][2]));
	}
}