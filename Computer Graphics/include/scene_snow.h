#pragma once
#define _USE_MATH_DEFINES
#include "scene.h"
#include "mat4.h"
#include "mat3.h"
#include "vec3.h"
#include <IL/il.h>
#include <math.h>
#include <tuple>
#include <iostream>
#include <vector>
// Escena de prueba para comenzar a trabajar con
// fragment shaders.
class scene_snow : public scene
{
public:
	~scene_snow();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

	float random();
	void actualizarParticula(int i);
	float actualizarLifeSpan();
	void initMatrices();
	void crearParticula();
	int current(int i);
	void sortParticles();


private:
	GLuint shader_program;

	GLuint vao;
	GLuint positionsVBO;

	GLuint texVBO;

	GLuint normVBO;
	GLuint indicesBuffer;

	ILuint imageID;
	GLuint textureId;


	//listas de posiciones
	std::vector<cgmath::vec3> positions;
	std::vector<cgmath::vec3> currPositions;

	//lista de velocidad
	std::vector<cgmath::vec3> velocity;
	//lista de tiempo de vida
	std::vector<float> lifeSpan;
	//magnitud-indice
	std::vector<std::tuple<float, int>> indVect;
	//particulas activas
	std::vector<bool> active;

	//Matrices
	cgmath::mat4 matCam;
	cgmath::mat4 matVista;
	cgmath::mat4 matProy;
	cgmath::mat4 matModel;
	cgmath::mat4 escala;
	cgmath::mat4 traslacion;
	cgmath::mat4 rotationz;
	cgmath::mat4 rotationy;
	cgmath::mat4 rotationx;
	cgmath::mat4 demiMatModel;
	cgmath::mat4 rotationCamz;
	cgmath::mat4 rotationCamy;
	cgmath::mat4 rotationCamx;
	cgmath::mat4 rotacionPiso;
	cgmath::mat4 traslacionPiso;
	cgmath::mat4 escalaPiso;
	cgmath::mat4 modelPiso;

	cgmath::mat3 normal;
	cgmath::mat3 demiModel3;

	int parNum = 1000;
	int ratio = 500;
	float partsToAct = 0.0;
	//valores de varianza para las posiciones
	float
		initX = 0.0f,
		initY = 30.0,
		initZ = 0.0f,
		varX = 50.0,
		varY = 5.0,
		varZ = 50.0;
	//valores de varianza para las velocidades
	float varVel = 0.2f,
		baseVel = 0.4f;
	//valores para avanzar camara
	float velCamX = 0.0f,
		velCamZ = 0.0f;
	//valores para rotar camara
	float rotCamX = 0.0f,
		rotCamY = 0.0f;
	//velocidad en x
	float velX = 0.0f;
	//valores para la proyeccion
	float
		nearProj = 1.0f,
		farProj = 1000.0f,
		fov = 60 * (M_PI / 180.0f),
		aspectRatio = 1.0f,
		camX = 0.0f,
		camZ = 0.0f;
};
