#pragma once
#include "scene.h"

#include <vector>
#include "vec2.h"

class scene_chaikin : public scene
{
public:
	void init();
	void createTotoro();
	void leftEye();
	void rightEye();
	void baldSpot();
	void nose();
	void leftEar();
	void rightEar();
	void tummy();
	void rightPaws();
	void leftPaws();
	void leftPadding();
	void downPadding();
	void rightPadding();
	void bindTotoro(std::vector<std::vector<cgmath::vec2>> figure, GLuint* figureVao, GLuint* figurePositionsVBO);
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void chaikin(std::vector<cgmath::vec2> &line,int refTimes,bool circle);
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	//Manager de atributos original
	GLuint vao[43];
	// Este es el buffer con el atributo de posicion original
	GLuint positionsVBO[43];
	
	//Manager de atributos refinado
	GLuint refVao[43];
	// Este es el buffer con el atributo de posicion original
	GLuint refPositionsVBO[43];
	//tipo de la figura original
	GLenum primitiveType;

	//tipo de la figura refinada
	GLenum refPrimitiveType;

	//lista para guardar los puntos de la figura original
	std::vector<std::vector<cgmath::vec2>> totoro;

	//lista para guardar los puntos de la figura refinada
	std::vector<std::vector<cgmath::vec2>> refTotoro;

	//Booleanos para mostrar figuras
	int REFS = 6;
	bool orTotoro;
	bool refToto;

};

