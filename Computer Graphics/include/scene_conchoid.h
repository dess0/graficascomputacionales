#pragma once
#include "scene.h"

#include <vector>
#include "vec2.h"

class scene_conchoid : public scene
{
public:
	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void calculateConchoid();
	void resize(int width, int height) { }
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	//Manager de atributos
	GLuint vao;
	// Este es el buffer con el atributo de posicion
	GLuint positionsVBO;

	GLenum primitiveType;

	//lista para guardar los puntos
	std::vector<cgmath::vec2> line;

	//constantes de la funcion
	float 
		a = 1.0f,
		b = 2.0f,
		scale = 10.0f,
		delta = 0.1f;
};