#pragma once
#define _USE_MATH_DEFINES
#include "scene.h"
#include "mat4.h"
#include <IL/il.h>
#include <math.h>
// Escena de prueba para comenzar a trabajar con
// fragment shaders.
class scene_cube : public scene
{
public:
	~scene_cube();

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key) { }
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:
	GLuint shader_program;

	GLuint vao;
	GLuint positionsVBO;
	//para los colores
	GLuint colorsVBO;
	GLuint indicesBuffer;
	GLuint indicesPiso;

	GLuint texVBO;
	ILuint imageCrateID;
	GLuint textureCrateId;

	GLuint texturePigId;
	ILuint imagePigID;

	//Matrices
	cgmath::mat4 matVista;
	cgmath::mat4 matProy;
	cgmath::mat4 matModel;
	cgmath::mat4 escala;
	cgmath::mat4 traslacion;
	cgmath::mat4 rotationz;
	cgmath::mat4 rotationy;
	cgmath::mat4 rotationx;

	//valores para la proyeccion
	float 
		nearProj = 1.0f,
		farProj = 1000.0f,
		fov = 60 * (M_PI / 180.0f),
		aspectRatio = 1.0f;

};
