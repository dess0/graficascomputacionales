#version 330

in vec4 InterpolatedColor;
uniform vec2 iResolution;

out vec4 FragColor;

void main()
{   
    vec2 pos=(gl_FragCoord.xy) / iResolution.xy;
	if(distance(pos,vec2(0.5f,0.5f))<0.25f)
		discard;
    FragColor = InterpolatedColor;
}