#version 330

in vec3 VertexPosition;
in vec4 Color;
out vec4 InterpolatedColor;

void main()
{
	gl_Position = vec4(VertexPosition, 1.0f);
	InterpolatedColor=Color;
}