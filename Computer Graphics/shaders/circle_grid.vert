#version 330

uniform float time;

out vec4 InterpolatedColor;

void main() {
  float width=10.0f;
  float poin=42.0f;
  
  float circleId=mod(gl_VertexID,poin);
  float x=floor(circleId / 2.0f);
  float y=mod(circleId+1.0f,2.0f);
  float ang=(x/((poin-2.0f)/2.0f))*radians(360.0f);
  float rad=2.0f-y;
  float xc= rad*cos(ang);
  float yc =rad*sin(ang);
  

  float gridId= floor(gl_VertexID/poin);
  float gridx = mod(gridId, width);
  float gridy = floor(gridId / width);
  float u = gridx / (width - 1.0f);
  float v = gridy / (width - 1.0f);
  
  float xOffset = cos(time + gridy * 0.2f) * 0.1f;
  float yOffset = cos(time + gridx * 0.3f) * 0.2f;
  
  float ux = u * 2.0f - 1.0f + xOffset+xc*0.05f;
  float vy = v * 2.0f - 1.0f + yOffset+yc*0.05f;
  
  vec2 xy = vec2(ux, vy) *1.2f;

  gl_Position =vec4(xy,0.0f,1.0f);
  gl_PointSize =10.0f;
  InterpolatedColor=vec4(0.0f,0.502f,0.506f,1.0f);
}