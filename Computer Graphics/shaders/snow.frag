#version 330

in vec2 texCoord;
in vec3 NormalF;
in vec3 FragPosition;

uniform vec3 lightColor;
uniform vec3 lightPosition;
uniform vec2 iResolution;
uniform sampler2D ejemploTextura;
uniform vec3 ViewPosition;

out vec4 FragColor;

void main()
{   
   float ambientStrength = 0.16;
    vec3 ambient = ambientStrength * lightColor;

	vec3 norm = normalize(NormalF);
	vec3 lightDir = normalize(lightPosition - FragPosition); 
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;

	float specularStrength = 0.5;
	vec3 viewDir = normalize(ViewPosition - FragPosition);
	vec3 reflectDir = reflect(-lightDir, norm); 
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * lightColor; 

    FragColor =  vec4((ambient + diffuse + specular), 1.0) * texture2D(ejemploTextura,texCoord);
}