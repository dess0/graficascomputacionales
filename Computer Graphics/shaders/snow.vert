#version 330

in vec3 VertexPosition;
in vec2 texPos;
in vec3 NormalVec;

uniform mat3 Normal;
uniform mat3 Model3;
uniform mat4 MvpMatrix;

out vec2 texCoord;
out vec3 NormalF;
out vec3 FragPosition;

void main()
{   texCoord=texPos;

	gl_Position = MvpMatrix * vec4(VertexPosition, 1.0f);
	NormalF = Normal * NormalVec ;
	FragPosition= Model3 * VertexPosition;
}