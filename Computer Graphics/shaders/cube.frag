#version 330

in vec2 texCoord;
uniform vec2 iResolution;
uniform sampler2D ejemploTexturaCrate;
uniform sampler2D ejemploTexturaPig;

out vec4 FragColor;

void main()
{   
    vec4 crate = texture2D(ejemploTexturaCrate, texCoord);
	vec4 pig = texture2D(ejemploTexturaPig, texCoord);
	FragColor = mix(crate, pig, 0.5f);
}