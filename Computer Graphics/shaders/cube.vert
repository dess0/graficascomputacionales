#version 330

in vec3 VertexPosition;
in vec4 Color;
in vec2 texPos;
uniform mat4 MvpMatrix;
out vec2 texCoord;

void main()
{   texCoord=texPos;
	gl_Position = MvpMatrix * vec4(VertexPosition, 1.0f);
}