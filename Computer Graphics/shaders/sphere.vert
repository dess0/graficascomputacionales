#version 330

uniform float time;

out vec4 InterpolatedColor;

void main(){
  float r = 0.750f;
  float points =30.0f;
  
  float phi= floor(gl_VertexID / points)/points*radians(360.0f);
  float tetha=mod(gl_VertexID,points)/points *radians(360.0f);
  
  float x=r*sin(phi)*cos(tetha);
  float y=r*sin(phi)*sin(tetha);
  float z=r*cos(phi);

  float rotx=cos(time)*x + sin(time)*z;
  float roty= -sin(time)*-sin(time)*x + cos(time)*y+ -sin(time)*cos(time)*z;
  float rotz=cos(time)*-sin(time)*x + sin(time)*y + cos(time)*cos(time)*z;
  vec3 xy = vec3(rotx, roty,rotz ) * 1.2f;
  
  gl_Position = vec4(xy, 1.0f);
  InterpolatedColor=vec4(0.0f,0.502f,0.506f,1.0f);
  gl_PointSize=10.0f;
}
