#define _USE_MATH_DEFINES
#include "scene_fire.h"

#include "ifile.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include "mat3.h"
#include <math.h>
#include <IL/il.h>
#include <tuple>
#include <algorithm>

#include <iostream>
#include <vector>

bool sortAuxF(const std::tuple<float, int> a, const std::tuple<float, int>& b) {
	return (std::get<0>(a) > std::get<0>(b));
}

scene_fire::~scene_fire()
{
	glDeleteProgram(shader_program);
}

float scene_fire::random() {
	float ran = (float)rand() / (float)RAND_MAX;
	return -1 + ran * (1 - (-1));
}

void scene_fire::actualizarParticula(int i) {
	active[i] = true;
	//posicion
	float posX = velCamX + random()*varX;
	float posY = initY + random()*varY;
	float posZ = initZ + random()*varZ;

	currPositions[i] = cgmath::vec3(posX, posY, posZ);
	//velocidad
	velocity[i] = cgmath::vec3(velX, -(baseVel + varVel * random()), velZ);
	lifeSpan[i] = actualizarLifeSpan();
}
void scene_fire::sortParticles() {
	//llenamos tuplas parts
	for (int i = 0; i < parNum; i++) {
		cgmath::vec3 dist = cgmath::vec3(traslacion[3][0], traslacion[3][1], traslacion[3][2]) - currPositions[i];
		std::get<0>(indVect[i]) = dist.magnitude();
		std::get<1>(indVect[i]) = i;
	}
	std::sort(indVect.begin(), indVect.end(), sortAuxF);
}

void scene_fire::crearParticula() {
	for (int i = 0; i < parNum; i++) {
		currPositions.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
		velocity.push_back(cgmath::vec3(0.0f, 0.0f, 0.0f));
		lifeSpan.push_back(0.0);
		active.push_back(false);
		indVect.push_back(std::make_tuple(-1.0f, 0));
	}
}
float scene_fire::actualizarLifeSpan() {
	float baseT = 10.0;
	float varT = 0.5;
	return baseT + random()*varT;
}
void scene_fire::initMatrices() {
	rotationx = cgmath::mat4(1.0f);

	rotationy = cgmath::mat4(1.0f);

	rotationz = cgmath::mat4(1.0f);

	rotationCamz = cgmath::mat4(1.0f);


	matCam = cgmath::mat4(cgmath::vec4(1.0f, 0.f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 10.0f, 1.0f));

	escala = cgmath::mat4(1.0f);

	traslacion = cgmath::mat4(1.0f);

	demiMatModel = rotationz * rotationy * rotationx * escala;

	matProy = cgmath::mat4(cgmath::vec4(1 / (aspectRatio * tan(fov / 2)), 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 1 / (tan(fov / 2)), 0.0f, 0.0f), cgmath::vec4(0.0f, 0.0f, -((farProj + nearProj) / (farProj - nearProj)), -1.0f),
		cgmath::vec4(0.0f, 0.0f, -((2 * farProj * nearProj) / (farProj - nearProj)), 1.0f));

	rotacionPiso = cgmath::mat4(1.0f);
	traslacionPiso = cgmath::mat4(1.0f);
	escalaPiso = cgmath::mat4(1.0f);

	modelPiso = rotacionPiso * escalaPiso * traslacionPiso;
}

int scene_fire::current(int i) {
	return std::get<1>(indVect[i]);
}


void scene_fire::init()
{
	// definir las matrices 
	initMatrices();
	//inicializamos las posiciones del triangulo que seran las particulas 
	positions.push_back(cgmath::vec3(-0.5f, -0.5f, 0.0f));
	positions.push_back(cgmath::vec3(0.5f, -0.5f, 0.0f));
	positions.push_back(cgmath::vec3(0.0f, 0.5f, 0.0f));
	//creamos la lista de vector normal para el phong
	std::vector<cgmath::vec3> normalVec;
	cgmath::vec3 vn0 = cgmath::vec3(0.5f, -0.5f, 0.0f) - cgmath::vec3(-0.5f, -0.5f, 0.0f);
	cgmath::vec3 vn1 = cgmath::vec3(0.0f, 0.5f, 0.0f) - cgmath::vec3(-0.5f, -0.5f, 0.0f);
	cgmath::vec3 vectorN = cgmath::vec3().cross(vn0, vn1);
	normalVec.push_back(vectorN);
	normalVec.push_back(vectorN);
	normalVec.push_back(vectorN);
	//creamos los arrays con los valores de las particulas
	crearParticula();


	//posiciones textura
	std::vector<cgmath::vec2> texturePos;
	//cara1
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(0.5f, 1.0f));


	// Creacion y activacion del vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Creacion y configuracion del buffer del atributo de posicion
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Creacion y configuracion del buffer del atributo de textura
	glGenBuffers(1, &texVBO);
	glBindBuffer(GL_ARRAY_BUFFER, texVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * texturePos.size(), texturePos.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Creacion y configuracion del buffer del atributo de textura
	glGenBuffers(1, &normVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * normalVec.size(), normalVec.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	ifile shader_file;

	shader_file.read("shaders/fire.vert");

	std::string vertex_source = shader_file.get_contents();

	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);

	glCompileShader(vertex_shader);

	// Revision de errores de compilacion del vertex shader
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	// Repetimos el mismo proceso, pero ahora para un
	// fragment shader 
	shader_file.read("shaders/fire.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	// El identificador del shader lo creamos pero para un 
	// shader de tipo fragment.
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	// Revision de errores de compilacion del fragment shader
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}
	//

	shader_program = glCreateProgram();

	glAttachShader(shader_program, vertex_shader);

	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "texPos");
	glBindAttribLocation(shader_program, 2, "NormalVec");
	glLinkProgram(shader_program);


	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);


	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program,
		"iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);

	GLuint light_Color = glGetUniformLocation(shader_program, "lightColor");
	glUniform3f(light_Color, 1.0f, 1.0f, 1.0f);

	GLuint light_position_location = glGetUniformLocation(shader_program, "lightPosition");
	glUniform3f(light_position_location, 0.0f, 10.0f, 20.0f);

	//obtenemos el uniform textura

//Primera imagen
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("images/fire3.png");

	// CARGAR LOS DATOS DE TEXTURA
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH),
		ilGetInteger(IL_IMAGE_HEIGHT),
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());

	// TEXTURE UNIFORM


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// BORRAR IMAGEN
	ilBindImage(0);
	ilDeleteImages(1, &imageID);

	glUseProgram(0);

}

void scene_fire::awake()
{
	glEnable(GL_BLEND);
	//glBlendFunc(GL_ONE, GL_ONE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_fire::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_fire::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);
	float anglex = (rotCamX* 30.0f * M_PI) / 180.0f;
	float angley = (rotCamY * 30.0f * M_PI) / 180.0f;

	rotationCamx = cgmath::mat4(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, cos(anglex), sin(anglex), 0.0f),
		cgmath::vec4(0.0f, -(sin(anglex)), cos(anglex), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	rotationCamy = cgmath::mat4(cgmath::vec4(cos(angley), 0.0f, -(sin(angley)), 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(sin(angley), 0.0f, cos(angley), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	matCam[3][0] = velCamX;
	matCam[3][2] = velCamZ;
	cgmath::mat4 camMod = rotationCamz * rotationCamy * rotationCamx *matCam;
	matVista = cgmath::mat4::inverse(camMod);

	cgmath::mat3 camMod3 = cgmath::mat3(cgmath::vec3(camMod[0][0], camMod[0][1], camMod[0][2]),
		cgmath::vec3(camMod[1][0], camMod[1][1], camMod[1][2]),
		cgmath::vec3(camMod[2][0], camMod[2][1], camMod[2][2]));
	cgmath::mat3 inv = cgmath::mat3::inverse(camMod3);
	normal = cgmath::mat3::transpose(inv);
	GLuint demiM = glGetUniformLocation(shader_program, "Model3");
	glUniformMatrix3fv(demiM, 1, GL_FALSE, &camMod3[0][0]);

	GLuint normalM = glGetUniformLocation(shader_program, "Normal");
	glUniformMatrix3fv(normalM, 1, GL_FALSE, &normal[0][0]);

	GLuint camara_position_location = glGetUniformLocation(shader_program, "ViewPosition");
	glUniform3f(camara_position_location, velCamX, 1.0f, velCamZ);

	//Dibujamos las gotas
	glBindVertexArray(vao);

	float dt = time::delta_time().count();
	partsToAct = ratio * dt;
	if (partsToAct > 1) {
		for (int i = 0; i < parNum; i++) {
			if (partsToAct < 1) {
				break;
			}
			else {
				if (!active[i]) {
					actualizarParticula(i);
					partsToAct -= 1;
				}
			}
		}
	}
	for (int i = 0; i < parNum; i++) {
		if (active[i]) {
			if (lifeSpan[i] < 0.0f) {
				active[i] = false;
			}
			else {
				velocity[i].x = velX;
				velocity[i].z = velZ;
				currPositions[i] = currPositions[i] - velocity[i] * (dt);
				lifeSpan[i] -= dt;
			}
		}

	}
	sortParticles();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);
	GLuint textMatCrate = glGetUniformLocation(shader_program, "ejemploTextura");
	glUniform1i(textMatCrate, 0);

	for (int i = 0; i < parNum; i++) {
		if (active[current(i)]) {
			traslacion[3][0] = currPositions[current(i)].x;
			traslacion[3][1] = currPositions[current(i)].y;
			traslacion[3][2] = currPositions[current(i)].z;
			//calculamos la matriz modelo

			cgmath::mat4 modelMatrix = demiMatModel * traslacion;

			cgmath::mat4 mvMatrix = matVista * modelMatrix;

			cgmath::mat4 mvMatBill = cgmath::mat4(cgmath::vec4(1.0f, 0.0f, 0.0f, mvMatrix[0][3]),
				cgmath::vec4(0.0f, 1.0f, 0.0f, mvMatrix[1][3]),
				cgmath::vec4(0.0f, 0.0f, 1.0f, mvMatrix[2][3]),
				cgmath::vec4(mvMatrix[3][0], mvMatrix[3][1], mvMatrix[3][2], mvMatrix[3][3]));
			cgmath::mat4 mvpMat = matProy * mvMatBill;
			//GLuint mvUnit = glGetUniformLocation(shader_program, "MvMat");
			GLuint mvpUnit = glGetUniformLocation(shader_program, "MvpMatrix");

			//glUniformMatrix4fv(mvUnit, 1, GL_FALSE, &mvMatrix[0][0]);
			glUniformMatrix4fv(mvpUnit, 1, GL_FALSE, &mvpMat[0][0]);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);
		}
	}
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}
void scene_fire::normalKeysDown(unsigned char key) {
	//avanzar camara
	if (key == 'w') velCamZ -= 0.4f;
	if (key == 'a') velCamX -= 0.4f;
	if (key == 's') velCamZ += 0.4f;
	if (key == 'd') velCamX += 0.4f;

	//rotar camara
	if (key == '8') rotCamX += 0.2f;
	if (key == '4') rotCamY += 0.2f;
	if (key == '5') rotCamX -= 0.2f;
	if (key == '6') rotCamY -= 0.2f;

	//simular viento
	if (key == 'x') velX += 0.4;
	if (key == 'z') velX -= 0.4;

	//simular viento en Z
	if (key == 'c') velZ += 0.4;
	if (key == 'v') velZ -= 0.4;

	//agregar particulas al pool
	if (key == 'm') {
		if (ratio < 900) ratio += 50;
	}
	if (key == 'n') {
		if (ratio > 50) ratio -= 50;
	}

}

void scene_fire::resize(int width, int height)
{
	//actualizar matriz de proyeccion
	glViewport(0, 0, width, height);
	aspectRatio = (float)width / (float)height;
	glUseProgram(shader_program);
	GLuint resolution_location =
		glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
}

/*
Fuentes de inspiracion
La explicacion del profesor en clase
Creditos a chan por ayudarme a aterrizar la idea para sortear.
Creditos a Nakakawa por ayudarme a entender el concepto de utilizar un enfoque de datos.
Creditos a Ray por ayudarme a entender la camara movil
https://www.geeksforgeeks.org/sort-c-stl/

Para 3ra entrega:
Creditos a naka por explicarme lo del pool y ayudarme a implementar una version mas dificil que sirvio para que entendiera mejor
al momento de implementar yo.
Creditos a Sandy por ayudarme a entender el phong shading
Guia para el phong shading
https://learnopengl.com/Lighting/Basic-Lighting?fbclid=IwAR38g6DDYBb9kG2CigJEtYVEEZiIyZd9FGiyE4UMfYFJvzp2duYPQX837ME

Guias extra
https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBindTexture.xhtml
https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glUniform.xhtml
https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGenVertexArrays.xhtml
https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glBindAttribLocation.xml

*/