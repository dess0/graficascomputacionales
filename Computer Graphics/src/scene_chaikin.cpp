#include "..\include\scene_chaikin.h"

void scene_chaikin::init()
{
	createTotoro();
	bindTotoro(totoro, vao, positionsVBO);
	bindTotoro(refTotoro, refVao, refPositionsVBO);
	refPrimitiveType = GL_LINE_STRIP;
	primitiveType = GL_LINES;

	orTotoro = true;
	refToto = true;
}

void scene_chaikin::createTotoro()
{
	leftEye();
	rightEye();
	nose();
	baldSpot();
	leftEar();
	rightEar();
	tummy();
	rightPaws();
	leftPaws();
	leftPadding();
	downPadding();
	rightPadding();
}

void scene_chaikin::leftEye()
{
	std::vector<cgmath::vec2> eye;
	std::vector<cgmath::vec2> eyeIn;
	//Afuera:
	eye.push_back(cgmath::vec2(-0.288903, 0.336288));
	eye.push_back(cgmath::vec2(-0.396816, 0.281649));
	eye.push_back(cgmath::vec2(-0.396816, 0.129078));
	eye.push_back(cgmath::vec2(-0.180015, 0.129078));
	eye.push_back(cgmath::vec2(-0.180015, 0.281649));
	eye.push_back(cgmath::vec2(-0.288903, 0.336288));

	//Adentro:

	eyeIn.push_back(cgmath::vec2(-0.293126, 0.253042));
	eyeIn.push_back(cgmath::vec2(-0.329796, 0.240206));
	eyeIn.push_back(cgmath::vec2(-0.329796, 0.180197));
	eyeIn.push_back(cgmath::vec2(-0.260475, 0.180197));
	eyeIn.push_back(cgmath::vec2(-0.260475, 0.240206));
	eyeIn.push_back(cgmath::vec2(-0.293126, 0.253042));

	totoro.push_back(eye);
	totoro.push_back(eyeIn);

	chaikin(eye, REFS, true);
	chaikin(eyeIn, REFS, true);
	refTotoro.push_back(eye);
	refTotoro.push_back(eyeIn);
}

void scene_chaikin::rightEye()
{
	std::vector<cgmath::vec2> eye;
	std::vector<cgmath::vec2> eyeIn;
	//Afuera:
	eye.push_back(cgmath::vec2(0.324584, 0.273971));
	eye.push_back(cgmath::vec2(0.221450, 0.219332));
	eye.push_back(cgmath::vec2(0.221450, 0.066761));
	eye.push_back(cgmath::vec2(0.438251, 0.066761));
	eye.push_back(cgmath::vec2(0.438251, 0.2193321));
	eye.push_back(cgmath::vec2(0.324584, 0.273971));

	//Adentro:

	eyeIn.push_back(cgmath::vec2(0.330283, 0.190725));
	eyeIn.push_back(cgmath::vec2(0.3, 0.177889));
	eyeIn.push_back(cgmath::vec2(0.3, 0.117880));
	eyeIn.push_back(cgmath::vec2(0.36931, 0.117880));
	eyeIn.push_back(cgmath::vec2(0.36931, 0.177889));
	eyeIn.push_back(cgmath::vec2(0.330283, 0.190725));

	totoro.push_back(eye);
	totoro.push_back(eyeIn);

	chaikin(eye, REFS, true);
	chaikin(eyeIn, REFS, true);
	refTotoro.push_back(eye);
	refTotoro.push_back(eyeIn);

}

void scene_chaikin::baldSpot()
{
	std::vector<cgmath::vec2> head;

	head.push_back(cgmath::vec2(0.223559, 0.397836));
	head.push_back(cgmath::vec2(0.072355, 0.471839));
	head.push_back(cgmath::vec2(-0.173719, 0.462589));

	totoro.push_back(head);
	chaikin(head, REFS, false);
	refTotoro.push_back(head);
}

void scene_chaikin::nose()
{
	std::vector<cgmath::vec2> upNose;
	//Telera:
	upNose.push_back(cgmath::vec2(0.070812, 0.183857));
	upNose.push_back(cgmath::vec2(0.066207, 0.208456));
	upNose.push_back(cgmath::vec2(-0.03093, 0.220599));
	upNose.push_back(cgmath::vec2(-0.03736, 0.164169));
	upNose.push_back(cgmath::vec2(0.059778, 0.156312));
	upNose.push_back(cgmath::vec2(0.070812, 0.183857));

	totoro.push_back(upNose);

	chaikin(upNose, REFS, true);
	refTotoro.push_back(upNose);
}

void scene_chaikin::leftEar()
{
	std::vector<cgmath::vec2> hair;
	std::vector<cgmath::vec2> ear;

	hair.push_back(cgmath::vec2(-0.4, 0.4));
	hair.push_back(cgmath::vec2(-0.352516, 0.439504));
	hair.push_back(cgmath::vec2(-0.417843, 0.439504));

	ear.push_back(cgmath::vec2(-0.122560, 0.495860));
	ear.push_back(cgmath::vec2(-0.104593, 0.517370));
	ear.push_back(cgmath::vec2(-0.093531, 0.599124));
	ear.push_back(cgmath::vec2(-0.102497, 0.712513));
	ear.push_back(cgmath::vec2(-0.129801, 0.833626));
	ear.push_back(cgmath::vec2(-0.162223, 0.923243));
	ear.push_back(cgmath::vec2(-0.252070, 0.922120));
	ear.push_back(cgmath::vec2(-0.298810, 0.844005));
	ear.push_back(cgmath::vec2(-0.367522, 0.698670));
	ear.push_back(cgmath::vec2(-0.396657, 0.549350));
	ear.push_back(cgmath::vec2(-0.376014, 0.481522));

	totoro.push_back(hair);
	totoro.push_back(ear);

	chaikin(ear, REFS, false);
	refTotoro.push_back(hair);
	refTotoro.push_back(ear);
}

void scene_chaikin::rightEar()
{
	std::vector<cgmath::vec2> hair;
	std::vector<cgmath::vec2> hair2;
	std::vector<cgmath::vec2> hair3;
	std::vector<cgmath::vec2> ear;

	hair.push_back(cgmath::vec2(0.494618, 0.328913));
	hair.push_back(cgmath::vec2(0.437015, 0.351314));
	hair.push_back(cgmath::vec2(0.476217, 0.312912));

	hair2.push_back(cgmath::vec2(0.307111, 0.411282));
	hair2.push_back(cgmath::vec2(0.25, 0.45));

	hair3.push_back(cgmath::vec2(0.276804, 0.395119));
	hair3.push_back(cgmath::vec2(0.238416, 0.417849));

	ear.push_back(cgmath::vec2(0.491452, 0.371081));
	ear.push_back(cgmath::vec2(0.528006, 0.392295));
	ear.push_back(cgmath::vec2(0.562689, 0.555773));
	ear.push_back(cgmath::vec2(0.530589, 0.736990));
	ear.push_back(cgmath::vec2(0.488112, 0.851865));
	ear.push_back(cgmath::vec2(0.409643, 0.852103));
	ear.push_back(cgmath::vec2(0.356050, 0.748010));
	ear.push_back(cgmath::vec2(0.285909, 0.571181));
	ear.push_back(cgmath::vec2(0.279723, 0.480777));
	ear.push_back(cgmath::vec2(0.298399, 0.462318));


	totoro.push_back(hair);
	totoro.push_back(hair2);
	totoro.push_back(hair3);
	totoro.push_back(ear);

	chaikin(ear, REFS, false);
	refTotoro.push_back(hair);
	refTotoro.push_back(hair2);
	refTotoro.push_back(hair3);
	refTotoro.push_back(ear);
}

void scene_chaikin::tummy()
{
	std::vector<cgmath::vec2> spot;
	std::vector<cgmath::vec2> spot2;
	std::vector<cgmath::vec2> spot3;
	std::vector<cgmath::vec2> tummyLine;

	spot.push_back(cgmath::vec2(-0.420643, -0.272545));
	spot.push_back(cgmath::vec2(-0.481833, -0.296399));
	spot.push_back(cgmath::vec2(-0.527467, -0.381443));
	spot.push_back(cgmath::vec2(-0.515021, -0.413594));
	spot.push_back(cgmath::vec2(-0.455905, -0.382480));
	spot.push_back(cgmath::vec2(-0.379158, -0.376257));
	spot.push_back(cgmath::vec2(-0.321291, -0.381640));
	spot.push_back(cgmath::vec2(-0.330413, -0.324401));
	spot.push_back(cgmath::vec2(-0.371898, -0.280842));
	spot.push_back(cgmath::vec2(-0.420643, -0.272545));

	spot2.push_back(cgmath::vec2(-0.144769, -0.276694));
	spot2.push_back(cgmath::vec2(-0.224627, -0.294325));
	spot2.push_back(cgmath::vec2(-0.283031, -0.372640));
	spot2.push_back(cgmath::vec2(-0.255114, -0.411812));
	spot2.push_back(cgmath::vec2(-0.193574, -0.375438));
	spot2.push_back(cgmath::vec2(-0.122989, -0.385591));
	spot2.push_back(cgmath::vec2(-0.061799, -0.425002));
	spot2.push_back(cgmath::vec2(-0.050272, -0.382188));
	spot2.push_back(cgmath::vec2(-0.077468, -0.306811));
	spot2.push_back(cgmath::vec2(-0.144769, -0.276694));

	spot3.push_back(cgmath::vec2(0.126994, -0.332479));
	spot3.push_back(cgmath::vec2(0.060909, -0.340550));
	spot3.push_back(cgmath::vec2(0.009958, -0.409662));
	spot3.push_back(cgmath::vec2(0.027187, -0.430121));
	spot3.push_back(cgmath::vec2(0.082601, -0.411680));
	spot3.push_back(cgmath::vec2(0.156920, -0.437444));
	spot3.push_back(cgmath::vec2(0.185512, -0.495421));
	spot3.push_back(cgmath::vec2(0.219667, -0.484985));
	spot3.push_back(cgmath::vec2(0.233436, -0.456577));
	spot3.push_back(cgmath::vec2(0.212082, -0.391391));
	spot3.push_back(cgmath::vec2(0.171317, -0.344647));
	spot3.push_back(cgmath::vec2(0.126994, -0.332479));

	tummyLine.push_back(cgmath::vec2(0.274930, -0.265024));
	tummyLine.push_back(cgmath::vec2(0.216746, -0.163685));
	tummyLine.push_back(cgmath::vec2(-0.101626, -0.057275));
	tummyLine.push_back(cgmath::vec2(-0.460763, -0.100807));
	tummyLine.push_back(cgmath::vec2(-0.559293, -0.220685));

	totoro.push_back(spot);
	totoro.push_back(spot2);
	totoro.push_back(spot3);
	totoro.push_back(tummyLine);

	chaikin(spot, REFS, true);
	chaikin(spot2, REFS, true);
	chaikin(spot3, REFS, true);
	chaikin(tummyLine, REFS, false);

	refTotoro.push_back(spot);
	refTotoro.push_back(spot2);
	refTotoro.push_back(spot3);
	refTotoro.push_back(tummyLine);
}

void scene_chaikin::rightPaws()
{
	std::vector<cgmath::vec2> upLine;
	std::vector<cgmath::vec2> upaw1;
	std::vector<cgmath::vec2> upaw2;
	std::vector<cgmath::vec2> upaw3;
	std::vector<cgmath::vec2> downLine;
	std::vector<cgmath::vec2> dpaw1;
	std::vector<cgmath::vec2> dpaw2;
	std::vector<cgmath::vec2> dpaw3;

	upLine.push_back(cgmath::vec2(0.288177, -0.534753));
	upLine.push_back(cgmath::vec2(0.250750, -0.514272));
	upLine.push_back(cgmath::vec2(0.230583, -0.430450));
	upLine.push_back(cgmath::vec2(0.246293, -0.306429));
	upLine.push_back(cgmath::vec2(0.289474, -0.237606));

	upaw1.push_back(cgmath::vec2(0.327104, -0.534753));
	upaw1.push_back(cgmath::vec2(0.289474, -0.578871));

	upaw2.push_back(cgmath::vec2(0.346568, -0.541241));
	upaw2.push_back(cgmath::vec2(0.334890, -0.591847));

	upaw3.push_back(cgmath::vec2(0.4, -0.6));
	upaw3.push_back(cgmath::vec2(0.377710, -0.543837));

	downLine.push_back(cgmath::vec2(0.462053, -0.528265));
	downLine.push_back(cgmath::vec2(0.440845, -0.556208));
	downLine.push_back(cgmath::vec2(0.401435, -0.559582));

	dpaw1.push_back(cgmath::vec2(0.177465, -0.985736));
	dpaw1.push_back(cgmath::vec2(0.173833, -0.945782));

	dpaw2.push_back(cgmath::vec2(0.298539, -0.976050));
	dpaw2.push_back(cgmath::vec2(0.269481, -0.936096));

	dpaw3.push_back(cgmath::vec2(0.388133, -0.957889));
	dpaw3.push_back(cgmath::vec2(0.343336, -0.916724));

	totoro.push_back(upLine);
	totoro.push_back(upaw1);
	totoro.push_back(upaw2);
	totoro.push_back(upaw3);
	totoro.push_back(downLine);
	totoro.push_back(dpaw1);
	totoro.push_back(dpaw2);
	totoro.push_back(dpaw3);

	chaikin(upLine, REFS, false);
	chaikin(downLine, REFS, false);

	refTotoro.push_back(upLine);
	refTotoro.push_back(upaw1);
	refTotoro.push_back(upaw2);
	refTotoro.push_back(upaw3);
	refTotoro.push_back(downLine);
	refTotoro.push_back(dpaw1);
	refTotoro.push_back(dpaw2);
	refTotoro.push_back(dpaw3);
}

void scene_chaikin::leftPaws()
{
	std::vector<cgmath::vec2> upLine;
	std::vector<cgmath::vec2> upaw1;
	std::vector<cgmath::vec2> upaw2;
	std::vector<cgmath::vec2> dpaw1;
	std::vector<cgmath::vec2> dpaw2;
	std::vector<cgmath::vec2> dpaw3;

	upLine.push_back(cgmath::vec2(-0.575205, -0.214066));
	upLine.push_back(cgmath::vec2(-0.562769, -0.268486));
	upLine.push_back(cgmath::vec2(-0.600175, -0.366850));
	upLine.push_back(cgmath::vec2(-0.640078, -0.401271));

	upaw1.push_back(cgmath::vec2(-0.680528, -0.415339));
	upaw1.push_back(cgmath::vec2(-0.688473, -0.442012));
	upaw1.push_back(cgmath::vec2(-0.656976, -0.465213));

	upaw2.push_back(cgmath::vec2(-0.709621, -0.412568));
	upaw2.push_back(cgmath::vec2(-0.731788, -0.433349));
	upaw2.push_back(cgmath::vec2(-0.715163, -0.461057));

	dpaw1.push_back(cgmath::vec2(-0.322418, -0.889986));
	dpaw1.push_back(cgmath::vec2(-0.362017, -0.942090));

	dpaw2.push_back(cgmath::vec2(-0.409954, -0.8670600));
	dpaw2.push_back(cgmath::vec2(-0.453722, -0.9170806));

	dpaw3.push_back(cgmath::vec2(-0.453722, -0.8316288));
	dpaw3.push_back(cgmath::vec2(-0.514163, -0.8816494));

	totoro.push_back(upLine);
	totoro.push_back(upaw1);
	totoro.push_back(upaw2);
	totoro.push_back(dpaw1);
	totoro.push_back(dpaw2);
	totoro.push_back(dpaw3);

	chaikin(upLine, REFS, false);
	chaikin(upaw1, REFS, false);
	chaikin(upaw2, REFS, false);

	refTotoro.push_back(upLine);
	refTotoro.push_back(upaw1);
	refTotoro.push_back(upaw2);
	refTotoro.push_back(dpaw1);
	refTotoro.push_back(dpaw2);
	refTotoro.push_back(dpaw3);
}

void scene_chaikin::leftPadding()
{
	std::vector<cgmath::vec2> upLine;
	std::vector<cgmath::vec2> whisker1;
	std::vector<cgmath::vec2> whisker2;
	std::vector<cgmath::vec2> downLine;
	std::vector<cgmath::vec2> padding;

	whisker1.push_back(cgmath::vec2(-0.478854, 0.224103));
	whisker1.push_back(cgmath::vec2(-0.547863, 0.293382));
	whisker1.push_back(cgmath::vec2(-0.629441, 0.330199));

	whisker2.push_back(cgmath::vec2(-0.514790, 0.184745));
	whisker2.push_back(cgmath::vec2(-0.570438, 0.216415));
	whisker2.push_back(cgmath::vec2(-0.637998, 0.224103));

	upLine.push_back(cgmath::vec2(-0.351251, 0.402272));
	upLine.push_back(cgmath::vec2(-0.483354, 0.315655));
	upLine.push_back(cgmath::vec2(-0.554576, 0.156508));

	downLine.push_back(cgmath::vec2(-0.554576, 0.156508));
	downLine.push_back(cgmath::vec2(-0.752776, -0.12654));
	downLine.push_back(cgmath::vec2(-0.742237, -0.397596));

	padding.push_back(cgmath::vec2(-0.702628, -0.494084));
	padding.push_back(cgmath::vec2(-0.650279, -0.697607));
	padding.push_back(cgmath::vec2(-0.486192, -0.822848));

	totoro.push_back(whisker1);
	totoro.push_back(whisker2);
	totoro.push_back(upLine);
	totoro.push_back(downLine);
	totoro.push_back(padding);

	chaikin(whisker1, REFS, false);
	chaikin(whisker2, REFS, false);
	chaikin(upLine, REFS, false);
	chaikin(downLine, REFS, false);
	chaikin(padding, REFS, false);

	refTotoro.push_back(whisker1);
	refTotoro.push_back(whisker2);
	refTotoro.push_back(upLine);
	refTotoro.push_back(downLine);
	refTotoro.push_back(padding);
}

void scene_chaikin::downPadding()
{
	std::vector<cgmath::vec2> low;
	std::vector<cgmath::vec2> tail;
	std::vector<cgmath::vec2> downLine;
	std::vector<cgmath::vec2> padding;

	downLine.push_back(cgmath::vec2(-0.444708, -0.846517));
	downLine.push_back(cgmath::vec2(-0.352116, -0.88655));
	downLine.push_back(cgmath::vec2(-0.129395, -0.92659));
	downLine.push_back(cgmath::vec2(0, -0.914084));

	low.push_back(cgmath::vec2(0, -0.914084));
	low.push_back(cgmath::vec2(0.138369, -0.941611));
	low.push_back(cgmath::vec2(0.366095, -0.909079));
	low.push_back(cgmath::vec2(0.421149, -0.836507));

	padding.push_back(cgmath::vec2(0.248478, -0.886556));
	padding.push_back(cgmath::vec2(0.341501, -0.854934));
	padding.push_back(cgmath::vec2(0.416521, -0.742404));
	padding.push_back(cgmath::vec2(0.421149, -0.653826));

	tail.push_back(cgmath::vec2(0.421149, -0.836507));
	tail.push_back(cgmath::vec2(0.530289, -0.857046));
	tail.push_back(cgmath::vec2(0.767230, -0.663148));
	tail.push_back(cgmath::vec2(0.768821, -0.328862));
	tail.push_back(cgmath::vec2(0.658251, -0.295063));

	totoro.push_back(tail);
	totoro.push_back(low);
	totoro.push_back(downLine);
	totoro.push_back(padding);

	chaikin(tail, REFS, false);
	chaikin(low, REFS, false);
	chaikin(downLine, REFS, false);
	chaikin(padding, REFS, false);

	refTotoro.push_back(tail);
	refTotoro.push_back(low);
	refTotoro.push_back(downLine);
	refTotoro.push_back(padding);
}

void scene_chaikin::rightPadding()
{
	std::vector<cgmath::vec2> upLine;
	std::vector<cgmath::vec2> whisker1;
	std::vector<cgmath::vec2> whisker2;
	std::vector<cgmath::vec2> downLine;

	whisker1.push_back(cgmath::vec2(0.728559, 0.104851));
	whisker1.push_back(cgmath::vec2(0.614009, 0.103153));
	whisker1.push_back(cgmath::vec2(0.508103, 0.061718));

	whisker2.push_back(cgmath::vec2(0.671048, 0.018586));
	whisker2.push_back(cgmath::vec2(0.596085, 0.035676));
	whisker2.push_back(cgmath::vec2(0.515291, 0.009001));

	upLine.push_back(cgmath::vec2(0.545378, 0.033237));
	upLine.push_back(cgmath::vec2(0.525701, 0.185567));
	upLine.push_back(cgmath::vec2(0.444998, 0.292960));

	downLine.push_back(cgmath::vec2(0.619655, -0.48948));
	downLine.push_back(cgmath::vec2(0.647048, -0.231975));
	downLine.push_back(cgmath::vec2(0.545378, 0.033237));

	totoro.push_back(whisker1);
	totoro.push_back(whisker2);
	totoro.push_back(upLine);
	totoro.push_back(downLine);

	chaikin(whisker1, REFS, false);
	chaikin(whisker2, REFS, false);
	chaikin(upLine, REFS, false);
	chaikin(downLine, REFS, false);

	refTotoro.push_back(whisker1);
	refTotoro.push_back(whisker2);
	refTotoro.push_back(upLine);
	refTotoro.push_back(downLine);
}

void scene_chaikin::bindTotoro(std::vector<std::vector<cgmath::vec2>> figure, GLuint* figureVao, GLuint* figurePositionsVBO)
{
	glGenVertexArrays(figure.size(), figureVao);
	glGenBuffers(figure.size(), figurePositionsVBO);
	for (int i = 0; i < figure.size(); i++) {
		//Creamos identificador para Vertex Array y trabajamos con el sig
		//glGenVertexArrays(1, &vao[i]);
		glBindVertexArray(figureVao[i]);

		//Creamos identificador para Vertex buffer object y empezar a trabajar
		//glGenBuffers(1, &positionsVBO[i]);
		glBindBuffer(GL_ARRAY_BUFFER, figurePositionsVBO[i]);
		glBufferData(GL_ARRAY_BUFFER, figure[i].size() * sizeof(cgmath::vec2), figure[i].data(), GL_DYNAMIC_DRAW);
		//Prendemos el attr 0
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		//Unbindear el buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
}


void scene_chaikin::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(1.0f);
	glLineWidth(1.0f);
}

void scene_chaikin::sleep()
{
	glPointSize(2.0f);
	glLineWidth(2.0f);
}
void scene_chaikin::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(orTotoro) {
		glColor3f(0.0, 1.0, 1.0);
		for (int i = 0; i < totoro.size(); i++) {
			glBindVertexArray(vao[i]);
			glDrawArrays(primitiveType, 0, totoro[i].size());
			glBindVertexArray(0);
		}
	}
	if (refToto) {
		glColor3f(1.0, 1.0, 1.0);
		for (int i = 0; i < refTotoro.size(); i++) {
			glBindVertexArray(refVao[i]);
			glDrawArrays(refPrimitiveType, 0, refTotoro[i].size());
			glBindVertexArray(0);
		}
	}
}

void scene_chaikin::chaikin(std::vector<cgmath::vec2> &line, int refTimes,bool circle)
{
	std::vector<cgmath::vec2> lineChaikin = line;
	
	for (int j = 0; j < refTimes; j++) {
		std::vector<cgmath::vec2> chaikinPass;
		if (!circle) {
			chaikinPass.push_back(lineChaikin.front());
		}
		for (int i = 0; i < lineChaikin.size() - 1; i++) {
			cgmath::vec2 qi = (0.75f*lineChaikin[i]) + (0.25f*lineChaikin[i + 1]);
			cgmath::vec2 ri = (0.25f*lineChaikin[i]) + (0.75f*lineChaikin[i + 1]);
			chaikinPass.push_back(qi);
			chaikinPass.push_back(ri);
		}
		if (circle) {
			chaikinPass.push_back(chaikinPass.front());
		}
		lineChaikin = chaikinPass;
	}
	if (!circle) {
		lineChaikin.push_back(line[line.size() - 1]);
	}
	line = lineChaikin;
}

void scene_chaikin::normalKeysDown(unsigned char key)
{
	if (key == 'o') {
		bool temp = !orTotoro;
		orTotoro = temp;
	};
	if (key == 'r') {
		bool temp = !refToto;
		refToto = temp;
	};
	//primitivas para la figura original
	if (key == '0') primitiveType = GL_POINTS;
	if (key == '5') primitiveType = GL_LINE_STRIP;
	if (key == '6') primitiveType = GL_LINES;
	//primitivas para la figura refinada
	if (key == '7') refPrimitiveType = GL_POINTS;
	if (key == '8') refPrimitiveType = GL_LINE_STRIP;
	if (key == '2') primitiveType = GL_LINES;
}
/*Fuentes de inspiraci�n:
http://docs.gl/gl4/glGenVertexArrays
http://docs.gl/gl4/glBindVertexArray
http://docs.gl/gl4/glGenBuffers
http://docs.gl/gl4/glBindBuffer
http://docs.gl/gl4/glBufferData
teor�a del segundo problema en pdf.

Cr�ditos a Cinth por darme la idea del dibujo
Cr�ditos a Ray por ayudarme a entender mejor OpenGL
Cr�ditos a Jalay y a C�mara por darme idea de d�nde era m�s f�cil tener bugs
*/