#include "scene_conchoid.h"
#include <iostream>


void scene_conchoid::init()
{
	calculateConchoid();
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);

	glBufferData(GL_ARRAY_BUFFER, line.size() * sizeof(cgmath::vec2), line.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	primitiveType = GL_LINE_STRIP;
	
}


void scene_conchoid::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(1.0f);
}

void scene_conchoid::sleep()
{
	glPointSize(1.0f);
}

void scene_conchoid::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Bind del VAO 
	glBindVertexArray(vao);
	// Llamada a dibujar
	// Tipo de primitiva
	// Desde que vertices se empieza a dibujar
	// Cuantos vertices se dibujan
	glDrawArrays(primitiveType, 0, line.size());
	// Unbind del VAO 
	glBindVertexArray(0);
}

void scene_conchoid::calculateConchoid()
{
	for (float y = -1.0f; y <= 3.0f; y += delta)
	{
		float x = sqrtf((((b*b) * (y*y)) / powf(y - a, 2)) - (y*y));

		line.push_back(cgmath::vec2(-x / scale, y / scale));
	}
	for (float y = 3.0f; y >= -1.0f; y -= delta)
	{
		float x = sqrtf((((b*b) * (y*y)) / powf(y - a, 2)) - (y*y));

		line.push_back(cgmath::vec2(x / scale, y / scale));
	}
}


void scene_conchoid::normalKeysDown(unsigned char key)
{
	if (key == '1') primitiveType = GL_POINTS;
	if (key == '2') primitiveType = GL_LINE_STRIP;
}

/*
Fuentes de inspiraci�n:
http://mathworld.wolfram.com/ConchoidofNicomedes.html
http://xahlee.info/SpecialPlaneCurves_dir/ConchoidOfNicomedes_dir/conchoidOfNicomedes.html

Cr�ditos a Chan por explicarme como podria comenzar a aterrizar la idea del algoritmo*/