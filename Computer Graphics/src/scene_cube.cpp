#define _USE_MATH_DEFINES
#include "scene_cube.h"

#include "ifile.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <math.h>
#include <IL/il.h>

#include <iostream>
#include <vector>


scene_cube::~scene_cube()
{
	glDeleteProgram(shader_program);
}

void scene_cube::init()
{
	//inicializamos las posiciones

	std::vector<cgmath::vec3> positions;
	//cara frente
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f)); //0
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f)); //1
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f)); //2
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f)); //3
	//lado der
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f)); //4
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f)); //5
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f)); //6
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f)); //7
	//lado arriba 
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f)); //8
	positions.push_back(cgmath::vec3(3.0f, 3.0f, 3.0f)); //9
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f)); //10
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f)); //11
	//lado izq 
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f)); //12
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f)); //13
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, 3.0f)); //14
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f)); //15
	//lado bajo
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f)); //16
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f)); //17
	positions.push_back(cgmath::vec3(3.0f, -3.0f, 3.0f)); //18
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, 3.0f)); //19
	//lado fondo
	positions.push_back(cgmath::vec3(3.0f, -3.0f, -3.0f)); //20
	positions.push_back(cgmath::vec3(-3.0f, -3.0f, -3.0f)); //21
	positions.push_back(cgmath::vec3(-3.0f, 3.0f, -3.0f)); //22
	positions.push_back(cgmath::vec3(3.0f, 3.0f, -3.0f)); //23
	//posiciones textura
	std::vector<cgmath::vec2> texturePos;
	//cara1
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));
	//cara2
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));
	//cara3
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));
	//cara4
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));
	//cara5
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));
	//cara6
	texturePos.push_back(cgmath::vec2(0.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 0.0f));
	texturePos.push_back(cgmath::vec2(1.0f, 1.0f));
	texturePos.push_back(cgmath::vec2(0.0f, 1.0f));


	std::vector<cgmath::vec4> colors;
	//color frente
	colors.push_back(cgmath::vec4(0.0f, 0.502f, 0.506f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.502f, 0.506f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.502f, 0.506f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.502f, 0.506f, 1.0f));
	//color derecha
	colors.push_back(cgmath::vec4(1.0f, 0.4f, 0.4f, 1.0f));
	colors.push_back(cgmath::vec4(1.0f, 0.4f, 0.4f, 1.0f));
	colors.push_back(cgmath::vec4(1.0f, 0.4f, 0.4f, 1.0f));
	colors.push_back(cgmath::vec4(1.0f, 0.4f, 0.4f, 1.0f));
	//color arriba
	colors.push_back(cgmath::vec4(0.835f, 1.0f, 0.502f, 1.0f));
	colors.push_back(cgmath::vec4(0.835f, 1.0f, 0.502f, 1.0f));
	colors.push_back(cgmath::vec4(0.835f, 1.0f, 0.502f, 1.0f));
	colors.push_back(cgmath::vec4(0.835f, 1.0f, 0.502f, 1.0f));
	//color izq
	colors.push_back(cgmath::vec4(0.0f, 0.4f, 0.2f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.4f, 0.2f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.4f, 0.2f, 1.0f));
	colors.push_back(cgmath::vec4(0.0f, 0.4f, 0.2f, 1.0f));
	//color abajo
	colors.push_back(cgmath::vec4(0.6f, 0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.6f, 0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.6f, 0.0f, 0.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.6f, 0.0f, 0.0f, 1.0f));
	//color fondo
	colors.push_back(cgmath::vec4(0.875f, 0.502f, 1.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.875f, 0.502f, 1.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.875f, 0.502f, 1.0f, 1.0f));
	colors.push_back(cgmath::vec4(0.875f, 0.502f, 1.0f, 1.0f));
	//definimos indices
	std::vector<unsigned int> indices = { 0,1,2,2,3,0,4,5,6,6,7,4,8,9,10,10,11,8,12,13,14,14,15,12,16,17,18,18,19,16,20,21,22,22,23,20 };

	// definir las matrices 
	matVista = cgmath::mat4::inverse(cgmath::mat4(cgmath::vec4(1.0f, 0.f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 10.0f, 1.0f)));
	escala = cgmath::mat4(1.0f);
	traslacion = cgmath::mat4(1.0f);

	// Creacion y activacion del vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Creacion y configuracion del buffer del atributo de posicion
	glGenBuffers(1, &positionsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Creacion y configuracion del buffer del atributo de color
	glGenBuffers(1, &colorsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec4) * colors.size(), colors.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Creacion y configuracion del buffer del atributo de textura
	glGenBuffers(1, &texVBO);
	glBindBuffer(GL_ARRAY_BUFFER, texVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * texturePos.size(), texturePos.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// A diferencia de los buffers de atributos, los buffers de indices deben permanecer activos. No hacemos unbind.
	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;

	shader_file.read("shaders/cube.vert");

	std::string vertex_source = shader_file.get_contents();

	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);

	glCompileShader(vertex_shader);

	// Revision de errores de compilacion del vertex shader
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	// Repetimos el mismo proceso, pero ahora para un
	// fragment shader contenido en un archivo llamado
	// solid_color.frag dentro de la carpeta shaders.
	shader_file.read("shaders/cube.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	// El identificador del shader lo creamos pero para un 
	// shader de tipo fragment.
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	// Revision de errores de compilacion del fragment shader
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}
//

	shader_program = glCreateProgram();

	glAttachShader(shader_program, vertex_shader);

	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "texPos");

	glLinkProgram(shader_program);


	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);


	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program,
		"iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);
	
	//Primera imagen
	ilGenImages(1, &imageCrateID);
	ilBindImage(imageCrateID);
	ilLoadImage("images/crate.png");

	// CARGAR LOS DATOS DE TEXTURA
	glGenTextures(1, &textureCrateId);
	glBindTexture(GL_TEXTURE_2D, textureCrateId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH),
		ilGetInteger(IL_IMAGE_HEIGHT),
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());

	// TEXTURE UNIFORM
	GLuint textMatCrate = glGetUniformLocation(shader_program, "ejemploTexturaCrate");
	glUniform1i(textMatCrate, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// BORRAR IMAGEN
	ilBindImage(0);
	ilDeleteImages(1, &imageCrateID);

	// SEGUNDA IMAGEN
	ilGenImages(1, &imagePigID);
	ilBindImage(imagePigID);
	ilLoadImage("images/pig.png");

	// CARGAR LOS DATOS DE TEXTURA
	glGenTextures(1, &texturePigId);
	glBindTexture(GL_TEXTURE_2D, texturePigId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_WIDTH),
		ilGetInteger(IL_IMAGE_HEIGHT),
		0,
		ilGetInteger(IL_IMAGE_FORMAT),
		ilGetInteger(IL_IMAGE_TYPE),
		ilGetData());

	// TEXTURE UNIFORM
	GLuint textMatPig = glGetUniformLocation(shader_program, "ejemploTexturaPig");
	glUniform1i(textMatPig, 1);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	// BORRAR IMAGEN
	ilBindImage(0);
	ilDeleteImages(1, &imagePigID);
	glUseProgram(0);
}

void scene_cube::awake()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_cube::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_cube::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureCrateId);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texturePigId);
	/*
	actualizar las matrices z y x
	*/
	//matrices de rotacion
	//sacamos los angulos con el tiempo
	float timeC = time::elapsed_time().count();

	float anglex = (timeC* 30.0f * M_PI) / 180.0f;
	float angley = (timeC * 60.0f * M_PI) / 180.0f;
	float anglez = (timeC* 30.0f * M_PI) / 180.0f;

	rotationx = cgmath::mat4(cgmath::vec4(1.0f, 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, cos(anglex), sin(anglex), 0.0f),
		cgmath::vec4(0.0f, -(sin(anglex)), cos(anglex), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	rotationy = cgmath::mat4(cgmath::vec4(cos(angley), 0.0f, -(sin(angley)), 0.0f),
		cgmath::vec4(0.0f, 1.0f, 0.0f, 0.0f),
		cgmath::vec4(sin(angley), 0.0f, cos(angley), 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	rotationz = cgmath::mat4(cgmath::vec4(cos(anglez), sin(anglez), 0.0f, 0.0f),
		cgmath::vec4(-(sin(anglez)), cos(anglez), 0.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 1.0f, 0.0f),
		cgmath::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	
	//calculamos la matriz modelo
	cgmath::mat4 modelMatrix = rotationz * rotationy * rotationx * escala * traslacion;

	//definimos la matriz de proyeccion para que se actualice en caso de resize
	matProy = cgmath::mat4(cgmath::vec4(1 / (aspectRatio * tan(fov / 2)), 0.0f, 0.0f, 0.0f),
		cgmath::vec4(0.0f, 1 / (tan(fov / 2)), 0.0f, 0.0f), cgmath::vec4(0.0f, 0.0f, -((farProj + nearProj) / (farProj - nearProj)), -1.0f),
		cgmath::vec4(0.0f, 0.0f, -((2 * farProj * nearProj) / (farProj - nearProj)), 1.0f));

	//definir la MVPMatrix (falta multiplicar por la model matrix)
	cgmath::mat4 mvpMatrix = matProy * matVista * modelMatrix;
	GLuint mvpUnit = glGetUniformLocation(shader_program, "MvpMatrix");
	glUniformMatrix4fv(mvpUnit,1,GL_FALSE,&mvpMatrix[0][0]);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);
}

void scene_cube::resize(int width, int height)
{
	//actualizar matriz de proyeccion
	glViewport(0, 0, width, height);
	aspectRatio = (float)width / (float)height;
	glUseProgram(shader_program);
	GLuint resolution_location =
		glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, width, height);
}

/*Creditos a Ray por darme la idea de girar el cubo en mi cabeza para sacar los puntos e indices por cara
Creditos de nuevo a Ray, Cinth y Jalay por ayudarme a entender mejor el concepto de las transformaciones
Para sacar las matrices de proyeccion, de rotaci�n y la mvp: creditos a la presentacion de bases matematicas y a la de espacios de transformaciones
Creditos a Chavez por pasarme la pagina que transforma de rgb convencional a decimal
Para el resize y pasar la matriz al shader:
http://docs.gl/gl4/glGenVertexArrays
http://docs.gl/gl4/glGenVertexArrays
*/