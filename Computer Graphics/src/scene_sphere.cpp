#include "scene_sphere.h"

#include "ifile.h"
#include "time.h"

#include <iostream>
#include <vector>

scene_sphere::~scene_sphere()
{
	glDeleteProgram(shader_program);
}

void scene_sphere::init()
{
	//Receta de cocina

	ifile shader_file;
	
	shader_file.read("shaders/sphere.vert");
	
	std::string vertex_source = shader_file.get_contents();
	
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	
	glCompileShader(vertex_shader);

	// Checamos errores
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	// Receta de cocina
	shader_file.read("shaders/solid_color.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	// El identificador del shader lo creamos pero para un 
	// shader de tipo fragment.
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	// Revision de errores de compilacion del fragment shader
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	//manager
	shader_program = glCreateProgram();
	
	glAttachShader(shader_program, vertex_shader);

	glAttachShader(shader_program, fragment_shader);
	//linkeamos
	glLinkProgram(shader_program);

	//borramos
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

void scene_sphere::awake()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_sphere::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_sphere::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);
	GLuint time_location = glGetUniformLocation(shader_program, "time");
	glUniform1f(time_location, time::elapsed_time().count());
	glDrawArrays(GL_POINTS, 0, 800);
	glUseProgram(0);
}

void scene_sphere::resize(int width, int height)
{
}
/*Creditos a C�mara por darme la idea de revisar las coordenadas esf�ricas
Matrices de rotacion obtenidas de presentaci�n de bases matematicas del curso
Coordenadas esfericas:
https://es.wikipedia.org/wiki/Coordenadas_esf%C3%A9ricas 
Dibujar: http://docs.gl/gl4/glGenVertexArrays */