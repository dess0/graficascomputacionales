#include "scene_circle_grid.h"

#include "ifile.h"
#include "time.h"

#include <iostream>
#include <vector>

scene_circle_grid::~scene_circle_grid()
{
	// Borramos la memoria cuando ya no existe la escena
	glDeleteProgram(shader_program);
}

void scene_circle_grid::init()
{
	//leemos archivos de texto
	ifile shader_file;
	shader_file.read("shaders/circle_grid.vert");
	std::string vertex_source = shader_file.get_contents();
	//cast a GLChar
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	//Identificador vertex shader
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	//enviamos el codigo fuente del shader
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	//compilamos codigo fuente
	glCompileShader(vertex_shader);

	//buscamos errores
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	//repetimos
	shader_file.read("shaders/solid_color.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	//ahora es un fragment
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	//buscamos errores
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	//creamos el manager
	shader_program = glCreateProgram();
	// Asociamos el shader con el manager
	glAttachShader(shader_program, vertex_shader);
	// En este caso, shader_program es manager de fragment_shader
	glAttachShader(shader_program, fragment_shader);
	// Ejecutamos el proceso de linkeo.
	glLinkProgram(shader_program);

	// Borramos los shaders, porque ya tenemos el ejecutable
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

void scene_circle_grid::awake()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_circle_grid::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_circle_grid::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader_program);
	GLuint time_location = glGetUniformLocation(shader_program, "time");
	glUniform1f(time_location, time::elapsed_time().count());
	for (int i = 0; i < 4200; i += 42) {
		glDrawArrays(GL_TRIANGLE_STRIP, i,42);
	}
	glUseProgram(0);
}

void scene_circle_grid::resize(int width, int height)
{
}
/*
Cr�ditos a Chavez y a Galv�n por ayudarme a entender mejor la idea de como se tiene que manejar los puntos
Dibujar la figura: http://docs.gl/gl4/glGenVertexArrays */