#include <iostream>
//Siempre que haya que utilizar OpenGL
//Hay que incluir esto en el mismo orden
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "time.h"
#include"scene_manager.h"


int main(int argc, char* argv[])
{
	scene_manager::start(argc, argv, "Hello World!", 400, 400);
	return 0;
}