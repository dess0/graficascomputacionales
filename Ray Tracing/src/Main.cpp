
#include <vector>

#include "image_writer.h"
#include "sphere.h"
#include "hitable_list.h"
#include "material.h"
#include"camera.h"

vec3 color(const ray& r, hitable *world,int depth) {
	hit_record rec;
	if (world->hit(r, 0.0001, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)) {
			return attenuation * color(scattered, world, depth + 1);
		}
		else {
			return vec3(0, 0, 0);
		}
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5*(unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
	}
}

hitable *random_scene() {
	int n = 600;
	hitable **list = new hitable*[n + 2];
	list[0] = new sphere(vec3(0, -1200, 0), 1200, new lambertian(vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++) {
		for (int b = -11; b < 11; b++) {
			float choose_mat = (float(rand()) / RAND_MAX);
			vec3 center(a + 0.9*(float(rand()) / RAND_MAX), 0.2, b + 0.9*(float(rand()) / RAND_MAX));
			if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
				if (choose_mat < 0.8) {
					list[i++] = new sphere(center, 0.2, new lambertian(vec3((float(rand()) / RAND_MAX)*(float(rand()) / RAND_MAX), (float(rand()) / RAND_MAX)*(float(rand()) / RAND_MAX), (float(rand()) / RAND_MAX)*(float(rand()) / RAND_MAX))));
				}
				else if (choose_mat < 0.95) {
					list[i++] = new sphere(center, 0.2, new metal(vec3(0.5*(1 + (float(rand()) / RAND_MAX)), 0.5*(1 + (float(rand()) / RAND_MAX)), 0.5*(1 + (float(rand()) / RAND_MAX))), 0.5*(float(rand()) / RAND_MAX)));
				}
				else {
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}
	
	list[i++] = new sphere(vec3(-8, 1, 0), 1.0, new lambertian(vec3(0.1, 0.0, 0.4)));
	list[i++] = new sphere(vec3(-4, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.3));
	list[i++] = new sphere(vec3(0, 1, 0), 1.0, new lambertian(vec3(0.4, 0.0, 0.6)));
	list[i++] = new sphere(vec3(4, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));
	return new hitable_list(list, i);
}

int main()
{
	int nx = 1200;
	int ny = 800;
	int ns = 100;

	std::vector<unsigned char> pixels;
	hitable *list[5];
	list[0] = new sphere(vec3(0, 0, -1), 0.5,new lambertian(vec3(0.8,0.3,0.3)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100,new lambertian(vec3(0.8,0.8,0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.3));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	list[4] = new sphere(vec3(-1, 0, -1), -0.45, new dielectric(1.5));
	/*hitable *list[2];
	float R = cos(M_PI / 4);
	list[0] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[1] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));
	hitable *world = new hitable_list(list, 2);
	camera cam(90,float(nx)/float(ny));*/
	hitable *world = random_scene();
	vec3 lookfrom(13, 2, 3);
	vec3 lookat(0, 0, 0);
	float dist_to_focus=10.0;
	float aperture = 0.1;
	camera cam(lookfrom, lookat, vec3(0,1,0),20,float(nx)/float(ny),aperture,dist_to_focus);
	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++) {
				float u = float(i + (float(rand()) / RAND_MAX)) / float(nx);
				float v = float(j + (float(rand()) / RAND_MAX)) / float(ny);
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color(r, world,0);
			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			pixels.push_back(int(255.99 * col[0]));
			pixels.push_back(int(255.99 * col[1]));
			pixels.push_back(int(255.99 * col[2]));
		}
	}

	image_writer::save_png("outFinal.png", nx, ny, 3, pixels.data());

	return 0;
}
/*Cr�ditos a Ray por ayudarme con la optimizaci�n y por ayudarme con el equivalente de drand48().
Cr�ditos a Sandy por darme una idea de d�nde buscar mis bugs.
Cr�ditos a la p�gina de https://stackoverflow.com/questions/1727881/how-to-use-the-pi-constant-in-c por la ayuda para usar M_PI*/